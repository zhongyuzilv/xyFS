package xy.FileSystem.Controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import xy.FileSystem.Entity.DiskfileRepository;
import xy.FileSystem.Propert.StorageProperties;


/**
 * @name: IndexController
 * @desc:
 * @date: 2020-11-25-10:53
 * @email: jobzhaobin@qq.com
 * @author: 赵滨
 */
@Controller
public class IndexController {

    @Autowired
    private DiskfileRepository repository;
    @Autowired
    private StorageProperties prop;

    @ApiOperation(value="用户首页，适配 H5")
    @GetMapping("/")
    public String index(ModelMap model){
        return "file/" + prop.getTemplate() + "/userIndex";
    }

    @ApiOperation(value="用户报告，适配 H5")
    @GetMapping("/report")
    public String report(ModelMap model,
                         @RequestParam(value = "tel",required = true,defaultValue = "") String tel,
                         @RequestParam(value = "fileType",required = true,defaultValue = "") Integer fileType){
        model.addAttribute("page", repository.findAllByObj(tel, fileType));
        model.addAttribute("tel", tel);
        model.addAttribute("fileType", fileType);
        return "file/" + prop.getTemplate() + "/userReport";
    }


}
