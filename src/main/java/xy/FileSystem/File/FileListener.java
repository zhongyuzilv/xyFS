package xy.FileSystem.File;

import java.io.UnsupportedEncodingException;

public interface FileListener {
	
	UploadResult Store(UploadFileExt ufe);
	String Download(String fileKeyorName) throws UnsupportedEncodingException;
	String Token(String key);
}
