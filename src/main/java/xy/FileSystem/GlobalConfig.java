package xy.FileSystem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import xy.FileSystem.Propert.StorageProperties;

import java.util.HashMap;

/**
 * @name: GlobalConfig
 * @desc:
 * @date: 2020-11-30-10:40
 * @email: jobzhaobin@qq.com
 * @author: 赵滨
 */
@ControllerAdvice

public class GlobalConfig {
    @Autowired
    private StorageProperties prop;

    @ModelAttribute
    public void addAttributes(Model model) {
        model.addAttribute("static", prop.getDomainOfStatic());
        model.addAttribute("bucket", prop.getDomainOfBucket());
    }
}
