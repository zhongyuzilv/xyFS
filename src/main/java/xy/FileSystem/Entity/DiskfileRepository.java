package xy.FileSystem.Entity;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DiskfileRepository extends JpaRepository<Diskfile, String> {
	
	Page<Diskfile> findByAppid(int appid, Pageable pageable);

	/** nativeQuery =true 表示这是原生SQL
	 * @param tel
	 * @param uploadUser
	 * @param pageable
	 * @return
	 */
	@Query(value = "SELECT * from diskfile where if(?1!='',tel=?1,1=1) and if(?2!='',upload_user=?2,1=1)", nativeQuery = true)
	Page<List<Diskfile>> findAllIfObj(String tel, String uploadUser, Pageable pageable);

	@Query(value = "SELECT * from diskfile where tel=? and file_type=? order by upload_date", nativeQuery = true)
	List<Diskfile> findAllByObj(String tel, Integer fileType);

	@Query(value = "SELECT * from diskfile where file_name=?", nativeQuery = true)
	Diskfile findByFileName(String fileName);
}